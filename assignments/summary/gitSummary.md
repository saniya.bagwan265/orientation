**Git** is a *version-control system* for tracking changes in computer files and coordinating work on those files among multiple people.
Git is a *Distributed Version Control System*. So Git does not necessarily rely on a central server to store all the versions of a project's files.
At this point, you can do all the basic local Git operations — creating or cloning a repository, making changes, staging and committing those changes, and viewing the history of all the changes the repository has been through. 
